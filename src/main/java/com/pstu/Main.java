package com.pstu;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static Scanner in = new Scanner(System.in);

    static App app = new App();

    public static void main(String[] args) throws FileNotFoundException {

        // module read by json (БЗ)
        Gson gson = new Gson();

        App appDB = gson.fromJson(new FileReader("db.json"), App.class);
        app = appDB;
        // end module


        loop();

    }

    private static void loop() {

        System.out.println("");
        System.out.println("----------------------------------");
        System.out.println("Выберите какое действие сделать:");
        System.out.println("1 - запрос Дорогих моделей телефонов");
        System.out.println("2 - запрос Черно-белых моделей телефонов");
        System.out.println("3 - запрос моделей меньше указанной Цены");
        Long action = in.nextLong();

        if (action.equals(1L)){
            List<Models> allModels = Arrays.asList(
                    app.getBigCost().getMemoryGreater70().getItems(),
                    app.getBigCost().getNewModel().getItems(),
                    app.getBigCost().getPlaying().getItems()
            );
            System.out.println("Найдены следующие модели: ");
            allModels.forEach(item2 -> System.out.println(item2.getName() + " Цена: " + item2.getPrice()));
        }
        if (action.equals(2L)){
            List<Models> allModels = Arrays.asList(
                    app.getSmallCost().getBlackWhite().getItems()
            );
            System.out.println("Найдены следующие модели: ");
            allModels.forEach(item2 -> System.out.println(item2.getName() + " Цена: " + item2.getPrice()));
        }
        if (action.equals(3L)){
            System.out.println("Введите меньше какой Цены искать");
            Long minPrice = in.nextLong();
            List<Models> allModels = Arrays.asList(
                    app.getSmallCost().getBlackWhite().getItems(),
                    app.getSmallCost().getClip().getItems(),
                    app.getSmallCost().getNoBlackWhite().getItems(),
                    app.getSmallCost().getNoClip().getItems(),
                    app.getBigCost().getMemoryGreater70().getItems(),
                    app.getBigCost().getNewModel().getItems(),
                    app.getBigCost().getPlaying().getItems()
            );
            System.out.println("Найдены следующие модели: ");
            allModels.stream()
                    .filter(item -> item.getPrice() <= minPrice)
                    .forEach(item2 -> System.out.println(item2.getName() + " Цена: " + item2.getPrice()));

        }



        loop();
    }



}
