package com.pstu;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SmallCost {
    private BlackWhite blackWhite;
    private NoBlackWhite noBlackWhite;
    private Clip clip;
    private NoClip noClip;
}
