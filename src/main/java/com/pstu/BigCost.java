package com.pstu;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BigCost {
    private Playing playing;
    private MemoryGreater70 memoryGreater70;
    private NewModel newModel;

}
