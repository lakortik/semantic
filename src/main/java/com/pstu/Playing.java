package com.pstu;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Playing {
    private Models items;
    private String processor;
}
